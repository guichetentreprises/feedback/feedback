package fr.ge.feedback.webclient.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.feedback.webclient.controller.HomeController;

/**
 * Tests {@link HomeController}.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class HomeControllerTest {

    /** mvc. */
    private MockMvc mvc;

    /** web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
    }

    /**
     * Tests {@link HomeController#home()}.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchPage() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("home/main"));
    }

}
