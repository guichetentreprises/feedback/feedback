package fr.ge.feedback.webclient.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller of the home page.
 *
 * @author Christian Cougourdan
 */
@Controller
public class HomeController {

    /**
     * Displays the home page.
     *
     * @return the name of the template
     */
    @RequestMapping("/")
    public String home() {
        return "home/main";
    }

}
