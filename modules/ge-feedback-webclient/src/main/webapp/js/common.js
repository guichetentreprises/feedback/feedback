require([ 'jquery', 'bootstrap', 'lib/polyfill', 'datatables.net' ], function($) {

    $('table[data-toggle="datatable"]').DataTable();

});
