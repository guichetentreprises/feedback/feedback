define([ 'jquery' ], function($) {

    var oldParamMethod = $.param;
    var rbrackect = /\[\]$/;

    function buildParams(prefix, value, add) {
        var key;
        if ($.isArray(value)) {
            $.each(value, function(i, v) {
                if (rbrackect.test(prefix)) {
                    add(prefix, v);
                } else {
                    buildParams(prefix + '[' + (typeof v === 'object' ? i : '') + ']', v, add);
                }
            });
        } else if (typeof value === "object") {
            for (key in value) {
                if (value.hasOwnProperty(key)) {
                    buildParams((prefix ? prefix + '.' : '') + key, value[key], add);
                }
            }
        } else {
            add(prefix, value);
        }
    }

    $.param = function(obj, traditional) {
        var prefix, lst = [], add = function(key, value) {
            if ($.isFunction(value)) {
                value = value();
            }
            lst.push(encodeURIComponent(key) + '=' + (null == value ? '' : encodeURIComponent(value)));
        };

        if (traditional || $.isArray(obj)) {
            return oldParamMethod(obj, traditional);
        } else if (!$.isArray(obj)) {
            buildParams(prefix, obj, add);
        }

        return lst.join('&');
    };

    return $.param;

});
