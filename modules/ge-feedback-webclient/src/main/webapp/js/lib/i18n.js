define([ 'jquery', 'lib/func/translate' ], function ($, translate) {

    var defaultLocale;
    var msgByLocale = {};

    var i18n = function (code) {
        var locale = (this.locale && this.locale.language) || defaultLocale;
        var args = Array.prototype.slice.call(arguments, 1);
        var res;
        if (locale) {
            var msg;
            if (msgByLocale[locale]) {
                msg = msgByLocale[locale][code];
            }
            if (!msg) {
                msg = code;
            }
            args.unshift(msg);
            res = translate.apply(this, args);
        } else {
            res = code;
        }
        return res;
    };

    i18n.register = function (fullLocale, messages) {
        if (!defaultLocale) {
            defaultLocale = fullLocale;
        }
        if (msgByLocale[fullLocale]) {
            msgByLocale[fullLocale] = $.extend(msgByLocale[fullLocale], messages);
        } else {
            msgByLocale[fullLocale] = messages;
        }
    }

    return i18n;

});
