define([ 'jquery', 'lib/func' ], function($, func) {

    var cache = {};

    var Template = function(name) {
        this._render = func.renderer($('#tpl-' + name).html());
    };

    Template.prototype.render = function(data) {
        return this._render(data);
    };

    Template.find = function(name) {
        var tpl = cache[name];
        if (!tpl) {
            tpl = cache[name] = new Template(name);
        }

        return tpl;
    };

    return Template;

});
