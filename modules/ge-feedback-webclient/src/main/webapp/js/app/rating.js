define([ 'jquery', 'jquery.barrating' ], function($) {

    var obj = {
        initialize : function(field, opts) {
            var root = $('select[name]', field);
            root.barrating({ theme : 'fontawesome-stars' });
        },
        validate : function(data, opts) {
            return {
                validated : true,
                value : data.asString()
            };
        }
    };

    return obj;
});