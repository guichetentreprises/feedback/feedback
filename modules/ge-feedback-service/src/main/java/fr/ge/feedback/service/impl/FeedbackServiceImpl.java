/**
 *
 */
package fr.ge.feedback.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.ibatis.session.RowBounds;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.feedback.service.IFeedbackService;
import fr.ge.feedback.service.bean.FeedbackBean;
import fr.ge.feedback.service.mapper.IFeedbackMapper;

/**
 * The Class FeedbackServiceImpl.
 *
 * @author bsadil
 */
public class FeedbackServiceImpl implements IFeedbackService {

    /** The feedback mapper. */
    @Autowired
    private IFeedbackMapper feedbackMapper;

    /** The dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * {@inheritDoc}
     */
    @Override
    public long create(final FeedbackBean entity) {
        return this.feedbackMapper.create(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long update(final FeedbackBean entity) {
        return this.feedbackMapper.update(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long deleteById(final Long id) {
        return this.feedbackMapper.deleteById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long deleteByPage(final String page) {
        return this.feedbackMapper.deleteByPage(page);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FeedbackBean findById(final Long id) {
        return this.feedbackMapper.findById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FeedbackBean> findByPage(final String page) {
        return this.feedbackMapper.findByPage(page);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <R> SearchResult<R> search(final SearchQuery searchQuery, final Class<R> expectedClass) {
        final Map<String, Object> filters = new HashMap<>();
        final List<SearchQueryOrder> orders = new ArrayList<>();
        if (null != searchQuery.getFilters()) {
            searchQuery.getFilters().forEach(filter -> filters.put(filter.getColumn(), filter));
        }

        if (null != searchQuery.getOrders()) {
            searchQuery.getOrders().forEach(order -> orders.add(new SearchQueryOrder(order.getColumn(), order.getOrder())));
        }

        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final List<FeedbackBean> entities = this.feedbackMapper.findAll(filters, orders, rowBounds);

        final SearchResult<R> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        if (null != entities) {
            searchResult.setContent(entities.stream().map(elm -> this.dozer.map(elm, expectedClass)).collect(Collectors.toList()));
        }

        searchResult.setTotalResults(this.feedbackMapper.count(filters));

        return searchResult;
    }

}
