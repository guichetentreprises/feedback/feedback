package fr.ge.feedback.ws.support.filter;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import java.util.UUID;

import javax.servlet.FilterChain;

import org.junit.Test;
import org.slf4j.MDC;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

/**
 *
 * @author Christian Cougourdan
 */
public class LogContextWsFilterTest {

    @Test
    public void testSimple() throws Exception {
        final String correlationId = UUID.randomUUID().toString();
        final String userId = "2018-11-JON-DOE-42";

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("X-Correlation-ID", correlationId);
        request.addHeader("X-User-ID", userId);

        final MockHttpServletResponse response = new MockHttpServletResponse();

        final FilterChain chain = (request1, response1) -> {
            assertThat(MDC.get("correlationId"), equalTo(correlationId));
            assertThat(MDC.get("userId"), equalTo(userId));
        };

        MDC.remove("correlationId");
        MDC.remove("userId");

        new LogContextWsFilter().doFilter(request, response, chain);

        assertThat(MDC.get("correlationId"), nullValue());
        assertThat(MDC.get("userId"), nullValue());
    }

    @Test
    public void testNone() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();

        final FilterChain chain = (request1, response1) -> {
            assertThat(MDC.get("correlationId"), notNullValue());
            assertThat(MDC.get("userId"), nullValue());
        };

        MDC.remove("correlationId");
        MDC.remove("userId");

        new LogContextWsFilter().doFilter(request, response, chain);

        assertThat(MDC.get("correlationId"), nullValue());
        assertThat(MDC.get("userId"), nullValue());
    }

    @Test
    public void testBadFormat() throws Exception {
        final String correlationId = UUID.randomUUID().toString();
        final String userId = "2018-11-JON-DOE-42";

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("X-Correlation-ID", userId);
        request.addHeader("X-User-ID", correlationId);

        final MockHttpServletResponse response = new MockHttpServletResponse();

        final FilterChain chain = (request1, response1) -> {
            assertThat(MDC.get("correlationId"), allOf(notNullValue(), not(equalTo(correlationId))));
            assertThat(MDC.get("userId"), nullValue());
        };

        MDC.remove("correlationId");
        MDC.remove("userId");

        new LogContextWsFilter().doFilter(request, response, chain);

        assertThat(MDC.get("correlationId"), nullValue());
        assertThat(MDC.get("userId"), nullValue());
    }

}
