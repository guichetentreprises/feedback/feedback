package fr.ge.feedback.ws.support.filter;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.container.PreMatching;

import org.slf4j.MDC;

/**
 * Filter to preserve logs.
 *
 * @author jpauchet
 */
@PreMatching
public class LogPreserveFilter implements ClientRequestFilter {

    /**
     * {@inheritDoc}
     */
    @Override
    public void filter(final ClientRequestContext requestContext) throws IOException {
        final String correlationId = MDC.get("correlationId");
        if (correlationId != null) {
            requestContext.getHeaders().putSingle("X-Correlation-ID", correlationId);
        }
        final String userId = MDC.get("userId");
        if (userId != null) {
            requestContext.getHeaders().putSingle("X-User-ID", userId);
        }
    }

}
